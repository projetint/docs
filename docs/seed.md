---
outline: deep
---
# Ajout de données initiales dans la bd (seed)

Le seed sont des données qui seront ajoutées dans votre base de données lors de sa création.

L'ajout d'un seed se fait dans un seul fichier qui se trouve dans la couche Persistence

1. Trouver LeNomDeVotreAppDbContextInitializer.cs dans Persistence
2. Créer une tâche asynchrone pour créer le seed de votre entité
3. Pour coder le seed : 
   1. Vous initialisez des objets typés de votre entité
   2. Vous validez s'ils sont déjà présents
   3. S'ils ne sont pas présents :
      1. Vous les ajoutez au contexte
      2. Vous sauvegardez le contexte en asynchrone

Voici un exemple pour une entité ingrédient 
```cs
private async Task SeedIngredients()
{
    List<Ingredient> seedIngredients = new List<Ingredient>();
    seedIngredients.Add(new Ingredient {
        Nom = "Poulet",
        Type = "Viande",
        Slug = "poulet"
    });
    seedIngredients.Add(new Ingredient
    {
        Nom = "Pétate",
        Type = "Légumes",
        Slug = "patate"
    });
    seedIngredients.Add(new Ingredient
    {
        Nom = "Pomme",
        Type = "Fruit",
        Slug = "pomme"
    });

    var existingIngredient = _context.Ingredients.IgnoreQueryFilters().FirstOrDefault(x => x.Nom == seedIngredients[0].Nom);
    if (existingIngredient != null)
        return;
    _context.Ingredients.AddRange(seedIngredients);
    await _context.SaveChangesAsync();
}
```

4. Toujours dans LeNomDeVotreAppDbContextInitializer.cs dans Persistence, trouver la méthode SeedASync
5. Ajouter un appel à votre méthode de création de seec

```cs
public async Task SeedAsync()
{
    try
    {
        await SeedRoles();
        await SeedUsersAndMembersForRole(Roles.ADMINISTRATOR);
        // Laisser le code déjà présent et ajouter un appel à votre méthode
        await SeedIngredients();
    }
    catch (Exception ex)
    {
        _logger.LogError(ex, "An error occurred while seeding the database.");
        throw;
    }
}
```

6. L'ajout des seeds se fera automatiquement lorsque vous lancerez l'application.

[Retour à la documentation](/stackAccueil)