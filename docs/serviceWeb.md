---
outline: deep
---
# Création du service de votre entité

### Création du type
1. Aller dans Web -> vue-app -> src -> types -> entities
2. Ajouter un fichier votreEntite.ts
3. Codez dans ce fichier une classe qui représente votre entité

Dans l'exemple, je conserve seulement ce que je voudrais afficher.
```ts
export class Ingredient {
    nom?: string;
    type?: string;
}
```
4. Exporter le type afin de le rendre accessible. Allez dans index .ts et ajouter l'exportation de votre type
```ts
export * from "./ingredient"
}
```

### Création de l'interface du service
5. Aller dans Web -> vue-app -> src -> injection
6. Ouvrir le fichier interfaces.ts
7. Ajouter l'importation de votre type
8. Ajouter l'interface pour votre (éventuel) service

Voici le fichier avec l'exemple complet de Rose. On voit aussi mon interface ajouté pour le lire tous d'ingrédient

```ts
import {
  ICreateBookRequest,
  IEditBookRequest,
} from "@/types/requests"
import {SucceededOrNotResponse} from "@/types/responses"
import {Ingredient, Book, IAuthenticatedMember, Member} from "@/types/entities"

export interface IApiService {
  headersWithJsonContentType(): any

  headersWithFormDataContentType(): any

  buildEmptyBody(): string
}

export interface IMemberService {
  getCurrentMember(): Promise<IAuthenticatedMember | undefined>
}

export interface IBookService {
  getAllBooks(): Promise<Book[]>

  getBook(bookId: string): Promise<Book>

  deleteBook(bookId: string): Promise<SucceededOrNotResponse>

  createBook(request: ICreateBookRequest): Promise<SucceededOrNotResponse>

  editBook(request: IEditBookRequest): Promise<SucceededOrNotResponse>
}

export interface IIngredientService {
    LireTousIngredients(): Promise<Ingredient[]>
}
```

7. Toujours dans Web -> vue-app -> src -> injection
8. Ouvrir le fichier types.ts
9. Ajouter une ligne pour rendre disponible l'interface de votre service

```ts
export const TYPES = {
  IApiService: Symbol.for("IApiService"),
  IMemberService: Symbol.for("IMemberService"),
  IBookService: Symbol.for("IBookService"),
  IIngredientService: Symbol.for("IIngredientService"),
  AxiosInstance: Symbol.for("AxiosInstance")
};
```

### Création du service

10. Aller dans Web -> vue-app -> src -> services
11. Créer un fichier votreEntitéService.ts
12. Ce fichier contiendra toutes les fonctionnalités du CRUD
13. Importez le type et l'interface
14. Copier le service bookService.ts de Rose et ajuster le code :smile:

```ts
import {IIngredientService} from "@/injection/interfaces";
import {injectable} from "inversify";
import {ApiService} from "./apiService";
import {AxiosError, AxiosResponse} from "axios";
import {Ingredient} from "@/types/entities";

@injectable()
export class IngredientService extends ApiService implements IIngredientService {
  public async LireTousIngredients(): Promise<Ingredient[]> {
    const response = await this
      ._httpClient
        .get<AxiosResponse<Ingredient[]>>(`${process.env.VUE_APP_API_BASE_URL}/ingredients`)
        .catch(function (error: AxiosError): AxiosResponse<Ingredient[]> {
            return error.response as AxiosResponse<Ingredient[]>
      })
      return response.data as Ingredient[]
  }

}
```

15. Toujours dans Web -> vue-app -> src -> services
16. Ouvrir index.ts
17. Ajouter l'exportation de votre service
```ts
export * from './ingredientService';
}
```

### Rendre le service disponible

15. Aller dans Web -> vue-app -> src
16. Ouvrir inversify.config.ts
17. S'assurer que l'interface de votre service est ajoutée partout
    
```ts
import {Container} from "inversify";
import axios, {AxiosInstance} from 'axios';
import "reflect-metadata";

import {TYPES} from "@/injection/types";
import {
  IApiService,
  IBookService,
  IMemberService,
//ici
  IIngredientService 
} from "@/injection/interfaces";
import {
  ApiService,
  BookService,
  MemberService,
//ici
  IngredientService 
} from "@/services";

const dependencyInjection = new Container();
dependencyInjection.bind<AxiosInstance>(TYPES.AxiosInstance).toConstantValue(axios.create())
dependencyInjection.bind<IApiService>(TYPES.IApiService).to(ApiService).inSingletonScope()
dependencyInjection.bind<IMemberService>(TYPES.IMemberService).to(MemberService).inSingletonScope()
dependencyInjection.bind<IBookService>(TYPES.IBookService).to(BookService).inSingletonScope()
//ici
dependencyInjection.bind<IIngredientService>(TYPES.IIngredientService).to(IngredientService).inSingletonScope() // ici

function useMemberService() {
  return dependencyInjection.get<IMemberService>(TYPES.IMemberService);
}

function useBookService() {
  return dependencyInjection.get<IBookService>(TYPES.IBookService);
}

//ici, créer la fonction useVotreService
function useIngredientService() {
    return dependencyInjection.get<IIngredientService>(TYPES.IIngredientService);
}

export {
  dependencyInjection,
  useMemberService,
  useBookService,
//ici
  useIngredientService
};
```



[Retour à la documentation](/stackAccueil)