---
outline: deep
---
# Création d'une liaison 1-N dans Domain

Voici un exemple d'une relation Équipe-Personne

```cs
public class Equipe : AuditableAndSoftDeletableEntity
{
    // Atttibuts de l'équipe (nom, logo, ...)
    public list<Personne> Membres { get; set; }
    
}

public class Personne : AuditableAndSoftDeletableEntity
{
    // Atttibuts de la personne (nom, prénom, ddn, ...)
    public Equipe Equipe { get; set; }
    
}
```

https://learn.microsoft.com/en-us/ef/core/modeling/relationships

# Création d'une liaison N-N dans Domain

La relation N-N peut être vu comme deux relations 1-N avec la création d'une entité comme table intermédiaire entre les deux entités.

Ainsi : 
1. Chacune des deux entités aura une relation 1-N vers l'entité de jonction.
2. L'entité de jonction aura deux objets typés de chacune des entités.

Voici un exemple d'une relation Ingredient-Recette.

```cs
 public class Ingredient : AuditableAndSoftDeletableEntity
 {
     public string Nom { get; set; } = default!;
     public string Slug { get; set; } = default!;
     public string Type { get; set; } = default!;
     public List<IngredientRecette> IngredientRecettes { get; set; } = default!;
 }

public class Recette : AuditableAndSoftDeletableEntity
{
    public string Nom { get; set; } = default!;
    public string Type { get; set; } = default!;
    public string Slug { get; set; } = default!;
    public int DureeEnMin { get; set; } = default!;
    public List<IngredientRecette> IngredientRecettes { get; set; } = default!;
}

public class IngredientRecette : AuditableAndSoftDeletableEntity
{
    public Ingredient Ingredient { get; set; } = default!;
    public Recette Recette { get; set; } = default!;
}
```
Une fois que votre relation N-N est en place, il est important de valider la migration afin de s'assurer que le modèle de BD généré est le bon.

pour plus d'info : https://learn.microsoft.com/en-us/ef/core/modeling/relationships/many-to-many


[Retour à la documentation](/stackAccueil)