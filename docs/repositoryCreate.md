---
outline: deep
---
# Ajustement du repository de l'entité

[Au besoin consulter la création du repository pour une entité](/repository.md)


### Ajustement de l'interface du repository
1. Dans Domain -> Repositories
2. Ajouter le Créer dans l'interface du repository de votre entité
3. Le créer sera une méthode qui retourne une Task et qui reçoit un ingrédient en paramètre

   
```cs
public interface IIngredientRepository
{
    List<Ingredient> LireTous();

    Task CreerIngredient(Ingredient ingredient);
}
```
### Ajustement du repository
1. Dans Infrastructure -> Repositories -> VotreEntité
2. Ajouter un using pour Slugify
3. Ajouter une fonction de génération pour le Slug. Le slug sera le champ utilisé dans l'url pour identifier chacune des instances de votre entité
4. Ajouter le créer dans votre repository
     
```cs
using Slugify;
public class IngredientRepository : IIngredientRepository
{
    public async Task CreerIngredient(Ingredient ingredient)
    {
        GenerateSlug(ingredient);
        _context.Ingredients.Add(ingredient);
        await _context.SaveChangesAsync();
    }
    private void GenerateSlug(Ingredient ingredient)
    {
        var slug = ingredient.Nom;
        var slugs = _context.Ingredients.AsNoTracking().Where(x => x.Slug == slug).ToList();
        if (slugs.Any())
            slug = $"{slug}-{slug.Length + 1}";
        ingredient.SetSlug(_slugHelper.GenerateSlug(slug).Replace(".", ""));
    }
}
```

[Retour à la documentation](/stackAccueil)