---
outline: deep
---
# Création du service aplicatif pour un Creer


### Création de l'interface du service
1. Aller dans Application -> Intrefaces -> Services
2. Créer un dossier au nom de votre entité
3. Ajouter une interface pour le service de la création de votre entité dans le dossier créé en #2
4. Définisser la méthode d'ajout dans l'interface


```cs
public interface IIngredientCreationService
{
    Task CreerIngredient(Ingredient ingredient);
}
```

### Création du service
5. Aller dans Application -> Intrefaces -> Services
6. Créer un dossier au nom de votre entité
7. Ajouter un service pour la création de votre entité dans le dossier créé en #6
   1. Ce service doit implémenter l'interface précédemment créée
   2. Donner accès au repository de votre entité
   3. Implémenter la méthode d'ajout

```cs
public class IngredientCreationService : IIngredientCreationService
{
    private readonly IIngredientRepository _ingredientRepository;

    public IngredientCreationService(IIngredientRepository ingredientRepository)
    {
        _ingredientRepository = ingredientRepository;
    }

    public async Task CreerIngredient(Ingredient ingredient)
    {

        await _ingredientRepository.CreerIngredient(ingredient);
    }
}
```

### Rendre le service disponible dans l'application (scoped)
8. Aller dans Application
9. Ouvrir ConfigureServices.cs
10. Ajouter votre service
    
```cs
public static class ConfigureServices
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());

        services.Configure<ApplicationSettings>(configuration.GetSection("Application"));

        services.AddScoped<ISlugHelper, SlugHelper>();

        services.AddScoped<IBookCreationService, BookCreationService>();
        services.AddScoped<IBookUpdateService, BookUpdateService>();
        services.AddScoped<INotificationService, EmailNotificationService>();
        services.AddScoped<IAuthenticatedUserService, AuthenticatedUserService>();
        services.AddScoped<IAuthenticatedMemberService, AuthenticatedMemberService>();

        // voici la ligne à ajouter :
        services.AddScoped<IIngredientCreationService, IngredientCreationService>();

        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        return services;
    }
}
```



[Retour à la documentation](/stackAccueil)