---
outline: deep
---
# Cours 1
# Planification
<!-- no toc -->
- [Présentation](#présentation)
- [Un cours particulier](#un-cours-particulier)
- [Mes attentes](#mes-attentes)
- [Mes rôles](#mes-rôles)
- [Formation des équipes](#formation-des-équipes)
- [Tp1](#tp1)
- [Présentation des projets](#présentation-des-projets)
- [Agile](#agile)
- [À faire en équipe](#à-faire-en-équipe)
- [À faire par personne](#à-faire-par-personne)

# Présentation

- Présentation personnelle
- Plan de cours : https://gitlab.com/projetint/contenu/-/blob/main/PlanCours.pdf?ref_type=heads
- Historique du cours
    - Deux cours avec un gros Tp
    - Deux cours avec un client factice
    - Un gros cours avec un client factice
    - Un gros cours avec un vrai client par équipe

# Un cours particulier
- Bienvenue chez GRNxSPK
  - On fusionne la frontière entre l'école et le marché du travail
  - Association entre Garneau TI / SPK + un client réel
  
  - SPK - Offre du temps d'une de leur meilleure dev, offre un service de soutien aux étudiants
  - SPK - Recoit une opportunité d'embaucher ceux/celles qui cadrent, permet de former les futurs devs selon leurs méthodes, permet de se faire connaître
  
  - Garneau TI - Offre un service de dev, offre l'encadrement dans la suivi de projet
  - Garneau TI - Recoit le meilleur environnement d'apprentissage pour des étudiants

  - Client - Offre une situation authentique et réelle sans garantie de résultat
  - Client - Reçoit (ou pas) une application gratis
- 
  - Pour vous c'est une opportunité de vous développer auprès d'une compagnie solide sans aucune pression.
  - Au final, les projets seront distribués au client. Une embauche ou même un fork est possible.
  - Un seul objectif dans tout ça : votre développement!

# Mes attentes
1. Etre en mesure de répondre aux besoins précis du client (humain)
    - Le client s'en #$%?& de votre algo propre de 8 lignes récursive.
    - Est-ce que ça fonctionne et est-ce que c'est ce que j'ai demandé ?
    - Je recherche totalement autre chose qu'en prog avancé et ce que vous étiez en prog avancé n'a pas vraiment de corrélation avec le cours de projet.
    - Si le client dit un gros bouton brun avec du comic ms, c'est votre responsabilité de ne pas le traiter de maudit moron et de le convaincre d'une autre solution, vous êtes le spécialiste de contenu.
    
 2. Livrer une application fonctionnelle et professionnelle
    - Le chemin pour arriver à la solution importe bcp moins qu'en prog avancé.
    - Couteau à double tranchant, en prog avancé je pouvais creuser dans le code/la logique pour donner des pts, pas en projet.
    - Je vais consulter le code sommairement, et la correction sera plus fonctionnelle. En gros, je clique partout, je tape avec des mitaines de four et je regarde si votre app brise.
    - Ne veut pas dire de ne pas faire de unit test et de coder comme dla marde

- Que vous assumiez votre statut de développeur, afin de combattre le syndrome de l'imposteur

# Mes rôles
1. Coach
2. Guide
3. Scrum master
4. PO
   
# Formation des équipes

- Parce que d'être un coéquipier ne signifie pas d'être capable de travailler efficacement toujours avec les mêmes personnes.
- Parce que prochainement vous ne déciderez pas de vos coéquipiers.
- Parce que il est important de développer votre capacité d'adaptation.
  
# Tp1

- Énoncé : https://gitlab.com/projetint/contenu/-/blob/main/Tp1.pdf?ref_type=heads

# Présentation des projets

1. Gestion de clients, Tableau de bord par client, Tableau de bord pour l'admin, commerce électronique. Attention ce projet sera aussi votre projet de Veille!
2. Visualisation de données. Excel -> BD, Permettre d'afficher et d'explorer les données, Permettre d'automatiquement cibler des corrélations de données.
3. Automatisation de processus. App de gestion de données, l'objectif principal est l'efficience pour l'utilisateur. Quelques utilisateurs qui feront une grosse utilisation de l'app.

# Agile
 - Agile vs Waterfall
 - SCRUM (devs)
 - Sprint (clients et devs)
 - Suivi de projet, suivi des dépenses, possibilité de modifier le projet
 - Tendre vers l'intégration continue
 - Exemple d'une maison 
   - Préfabriquée = Waterfall
   - Constructeur sur place avec suivi de chantier = Agile  
   - Possibilité de voir la maison se construire et de corriger le tir avec Agile
- Concept de découpage vertical
- Règles pour l'écriture des récits utilisateurs
  - Sujet, verbe, complément

# À faire en équipe
1. Trouver un nom d'équipe
2. Décider du mode de communication (discord, teams, slack, ...)
3. Remplir le contrat d'équipe : https://gitlab.com/projetint/contenu/-/blob/main/Contrat_equipe.docx?ref_type=heads
4. M'envoyer le contrat d'équipe par Teams
5. M'envoyer votre choix de projet par Teams
6. Préparer l'entrevue de mercredi

# À faire par personne
1. MaJ de Visual studio enterprise 2022
2. Installer SSMS (SQL server management studio)