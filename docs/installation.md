---
outline: deep
---
# Installation
<!-- no toc -->
## Installations par exécutables
- Install latest [SDK .NET Core 8](https://dotnet.microsoft.com/en-us/download/dotnet/8.0)
- Install [.NET EF CORE CLI tool](https://learn.microsoft.com/en-us/ef/core/cli/dotnet)
- Install [Sql Server](https://www.microsoft.com/en-ca/sql-server/sql-server-downloads)
- Install [SSMS] (https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16)
- Create dev user with rights to create tables :
  - In `Security` folder, right click on `Logins` and select `New login`
  - Select SQL Server authentication
  - Login name : `dev`
  - Password : `dev`
  - Click OK
- Install nvm

## Installation par le terminal
- Install node 21.7.3
    ```bash
    $ nvm install 21.7.3
    $ nvm use 21.7.3
    ```

## Dans la stack directement
- Cloner le [repo](https://gitlab.com/JpBoucher/stackprojeth25) 
- Restore nuget package dans le terminal de VS
    ```bash
    $ dotnet restore
    ```
- Run this command to generate JWT token secret key
    ```bash
    $ node -e "console.log(require('crypto').randomBytes(32).toString('hex'))"
    ```
- Copy the printed value to appsettings.Development.json JwtToken:SecretKey

- Installation des dépendances à partir du terminal de VS 
```bash
$ cd .\src\Web\vue-app
$ npm install yarn --global
$ Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
$ yarn install
```

- Installation de gulp à partir du terminal de VS
```bash
$ cd .\src\Web
$ npm install gulp
```

[Retour à la documentation](/stackAccueil)