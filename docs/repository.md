---
outline: deep
---
# Création du repository pour une entité

Petit rappel, le repository est l'accès au contenu de la BD. Vous pouvez voir un repository comme un entrepot qui permet de récupérer, stocker, supprimer ou modifier des données (CRUD).
Vous aurez normalement 5 fonctionnalités dans un repository :
1. Lire un élément de la BD
2. Lire tous les éléments de la BD
3. Créé un élément dans la BD
4. Mettre à jour un élément dans la BD
5. Supprimer un élément de la BD
   
À ces fonctionnalités s'ajoutera une méthode pour générer le slug. Le slug est une façon de formater les url afin d'éviter des caractères non-désirables et d'avoir des chemin d'accès plus proche du langage humain.

### Création du repository
1. Créer une interface dans Domain -> Repositories
2. Ajouter les fonctionnalités que votre application aura besoin d'effectuer sur votre entité

Pour l'exemple, nous ferons seulement un lire tous   
```cs
public interface IIngredientRepository
{
    List<Ingredient> LireTous();
}
```

Voici l'exemple de Rose qui contient toutes les fonctionnalités :
```cs
public interface IBookRepository
{
    List<Book> GetAll();
    Book FindById(Guid id);
    Task CreateBook(Book book);
    Task UpdateBook(Book book);
    Task DeleteBookWithId(Guid id);
}
```

3. Passer dans la couche Infrastructure
4. Créer un dossier au nom de votre entité dans Repositories
5. Créer une classe NomDeVotreEntitéRepository dans le dossier que vous venez de créer
6. Cette classe doit contenir au minimum
   1. Le contexte de la BD en attribut
   2. Un constructeur qui permet d'initialiser l'attribut du contexte de la BD
7. Ensuite, vous devez ajouter le code pour chacune de vos fonctionnalités
   
Pour l'exemple d'un lire tous   
```cs
public class IngredientRepository : IIngredientRepository
{
    private readonly GarneauTemplateDbContext _context;

    public IngredientRepository(GarneauTemplateDbContext context)
    {
        _context = context;
    }

    public List<Ingredient> LireTous()
    {
        return _context.Ingredients.AsNoTracking().ToList();
    }
}
```

Voici l'exemple de Rose qui contient toutes les fonctionnalités :
```cs
public class BookRepository : IBookRepository
{
    private readonly ISlugHelper _slugHelper;
    private readonly IGarneauTemplateDbContext _context;

    public BookRepository(IGarneauTemplateDbContext context, ISlugHelper slugHelper)
    {
        _context = context;
        _slugHelper = slugHelper;
    }

    public List<Book> GetAll()
    {
        return _context.Books.AsNoTracking().ToList();
    }

    public Book FindById(Guid id)
    {
        var book = _context.Books
            .AsNoTracking()
            .FirstOrDefault(x => x.Id == id);
        if (book == null)
            throw new BookNotFoundException($"Could not find book with id {id}.");
        return book;
    }

    public async Task CreateBook(Book book)
    {
        if (_context.Books.Any(x => x.Isbn.Trim() == book.Isbn.Trim()))
            throw new BookWithIsbnAlreadyExistsException($"A book with isbn {book.Isbn} already exists.");
        GenerateSlug(book);
        _context.Books.Add(book);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateBook(Book book)
    {
        if (_context.Books.Any(x => x.Isbn == book.Isbn.Trim() && x.Id != book.Id))
            throw new BookWithIsbnAlreadyExistsException($"Another book with isbn {book.Isbn} already exists.");

        if (string.IsNullOrWhiteSpace(book.Slug))
            GenerateSlug(book);

        _context.Books.Update(book);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteBookWithId(Guid id)
    {
        var book = _context.Books.FirstOrDefault(x => x.Id == id);
        if (book == null)
            throw new BookNotFoundException($"Could not find book with id {id}.");

        _context.Books.Remove(book);
        await _context.SaveChangesAsync();
    }

    private void GenerateSlug(Book book)
    {
        var slug = book.NameFr;
        var slugs = _context.Books.AsNoTracking().Where(x => x.Slug == slug).ToList();
        if (slugs.Any())
            slug = $"{slug}-{slug.Length + 1}";
        book.SetSlug(_slugHelper.GenerateSlug(slug).Replace(".", ""));
    }
}
```

8. Toujours dans la couche Infrastructure
9. Aller dans le fichier ConfigureServices.cs
10. Trouver la méthode ConfigureInfrastructureService.cs
11. Ajouter un service.AddScoped afin de rendre votre repository accessible dans les services
    
```cs
 private static void ConfigureInfrastructureServices(IServiceCollection services)
 {
     services.AddSingleton<IHttpContextUserService, HttpContextUserService>();

     services.AddScoped<IBookRepository, BookRepository>();
     services.AddScoped<IMemberRepository, MemberRepository>();
     services.AddScoped<IUserRepository, UserRepository>();
     services.AddScoped<IRoleRepository, RoleRepository>();

    // Voici la ligne que j'ai ajouté :
     services.AddScoped<IIngredientRepository, IngredientRepository>();

     services.AddScoped<IFileStorageApiConsumer, AzureBlobApiConsumer>();
     services.AddScoped<IAzureApiHttpClient, AzureApiHttpClient>();
     services.AddScoped<IAzureBlobWrapper, AzureBlobWrapper>();
 }
```


[Retour à la documentation](/stackAccueil)