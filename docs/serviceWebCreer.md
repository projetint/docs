---
outline: deep
---
# Création du service de votre entité

### Création du type

[Si ce n'est pas encore fait, créer votre type](/serviceWeb.md)

### Création de l'interface du service

[Si ce n'est pas encore fait, créer l'interface de votre service](/serviceWeb.md)
1. Ajouter l'action de création dans votre interace de service


Voici le fichier avec l'exemple complet de Rose et mon interface pour le lire tous d'ingrédient et le créer

```ts
import {
  ICreateBookRequest,
  IEditBookRequest,
} from "@/types/requests"
import {SucceededOrNotResponse} from "@/types/responses"
import {Ingredient, Book, IAuthenticatedMember, Member} from "@/types/entities"

export interface IApiService {
  headersWithJsonContentType(): any

  headersWithFormDataContentType(): any

  buildEmptyBody(): string
}

export interface IMemberService {
  getCurrentMember(): Promise<IAuthenticatedMember | undefined>
}

export interface IBookService {
  getAllBooks(): Promise<Book[]>

  getBook(bookId: string): Promise<Book>

  deleteBook(bookId: string): Promise<SucceededOrNotResponse>

  createBook(request: ICreateBookRequest): Promise<SucceededOrNotResponse>

  editBook(request: IEditBookRequest): Promise<SucceededOrNotResponse>
}

export interface IIngredientService {
    LireTousIngredients(): Promise<Ingredient[]>
    CreerIngredient(request: ICreateIngredientRequest): Promise<SucceededOrNotResponse>
}
```

2. Toujours dans Web -> vue-app -> src -> injection
3. Ouvrir le fichier types.ts
4. Vérifier que l'interface de votre service est bien présente, sinon l'ajouter.

```ts
export const TYPES = {
  IApiService: Symbol.for("IApiService"),
  IMemberService: Symbol.for("IMemberService"),
  IBookService: Symbol.for("IBookService"),
  IIngredientService: Symbol.for("IIngredientService"),
  AxiosInstance: Symbol.for("AxiosInstance")
};
```

5. Aller dans Web -> vue-app -> src
6. Ouvrir inversify.config.ts
7. S'assurer que l'interface de votre service est ajoutée partout
    
```ts
import {Container} from "inversify";
import axios, {AxiosInstance} from 'axios';
import "reflect-metadata";

import {TYPES} from "@/injection/types";
import {
  IApiService,
  IBookService,
  IMemberService,
//ici
  IIngredientService 
} from "@/injection/interfaces";
import {
  ApiService,
  BookService,
  MemberService,
//ici
  IngredientService 
} from "@/services";

const dependencyInjection = new Container();
dependencyInjection.bind<AxiosInstance>(TYPES.AxiosInstance).toConstantValue(axios.create())
dependencyInjection.bind<IApiService>(TYPES.IApiService).to(ApiService).inSingletonScope()
dependencyInjection.bind<IMemberService>(TYPES.IMemberService).to(MemberService).inSingletonScope()
dependencyInjection.bind<IBookService>(TYPES.IBookService).to(BookService).inSingletonScope()
//ici
dependencyInjection.bind<IIngredientService>(TYPES.IIngredientService).to(IngredientService).inSingletonScope() // ici

function useMemberService() {
  return dependencyInjection.get<IMemberService>(TYPES.IMemberService);
}

function useBookService() {
  return dependencyInjection.get<IBookService>(TYPES.IBookService);
}

//ici, créer la fonction useVotreService
function useIngredientService() {
    return dependencyInjection.get<IIngredientService>(TYPES.IIngredientService);
}

export {
  dependencyInjection,
  useMemberService,
  useBookService,
//ici
  useIngredientService
};
```

### Création du service

8. Aller dans Web -> vue-app -> src -> services
9. Si ce n'est pas déjà fait, Créer un fichier votreEntitéService.ts
10. Ce fichier contiendra toutes les fonctionnalités du CRUD
11. Importez le type et l'interface
12. Copier le service bookService.ts de Rose et ajuster le code :smile:

```ts
import {IIngredientService} from "@/injection/interfaces";
import {injectable} from "inversify";
import { ApiService } from "./apiService";
import { SucceededOrNotResponse } from "@/types/responses";
import { AxiosError, AxiosResponse } from "axios";
import { ICreateIngredientRequest } from "@/types/requests/createIngredientRequest";
import {Ingredient} from "@/types/entities";

@injectable()
export class IngredientService extends ApiService implements IIngredientService {
  public async LireTousIngredients(): Promise<Ingredient[]> {
    const response = await this
      ._httpClient
        .get<AxiosResponse<Ingredient[]>>(`${process.env.VUE_APP_API_BASE_URL}/api/ingredients`)
        .catch(function (error: AxiosError): AxiosResponse<Ingredient[]> {
            return error.response as AxiosResponse<Ingredient[]>
      })
      return response.data as Ingredient[]
    }

    public async CreerIngredient(request: ICreateIngredientRequest): Promise<SucceededOrNotResponse> {
        const response = await this
            ._httpClient
            .post<ICreateIngredientRequest, AxiosResponse<any>>(
                `${process.env.VUE_APP_API_BASE_URL}/api/ingredients`,
                request,
                this.headersWithFormDataContentType())
            .catch(function (error: AxiosError): AxiosResponse<SucceededOrNotResponse> {
                return error.response as AxiosResponse<SucceededOrNotResponse>
            })
        const succeededOrNotResponse = response.data as SucceededOrNotResponse
        return new SucceededOrNotResponse(succeededOrNotResponse.succeeded, succeededOrNotResponse.errors)
    }

}
```


[Retour à la documentation](/stackAccueil)