---
outline: deep
---
# Ajout de la route dans la couche Web

### Création de la route

1. Aller dans Web -> vue-app -> src -> locales
2. Ouvrir le fichier fr.json (vous n'aurez pas besoin de faire la version anglaise de votre site)
3. Ajouter la route que vous voulez créer

Dans l'exemple, je place ma route à dans admin, car c'est une fonctionnalité réservé aux admins.
```json
{
  "routes": {
    "home": {
      "path": "/fr",
      "fullPath": "/fr",
      "name": "Accueil"
    },
    "ingredients": {
      "path": "/fr/ingredients",
      "fullPath": "/fr/ingredients",
      "name": "Les ingredients"
    },
    "admin": {
      "path": "/fr/administration",
      "fullPath": "/fr/administration",
      "name": "Administration",
      "children": {
        "books": {
          "path": "livres",
          "fullPath": "/fr/administration/livres",
          "name": "Livres",
          "add": {
            "path": "ajouter",
            "fullPath": "/fr/administration/livres/ajouter",
            "name": "Ajouter un livre"
          },
          "edit": {
            "path": ":id/modifier",
            "fullPath": "/fr/administration/livres/:id/modifier",
            "name": "Modifier un livre"
          }
        },
        "ingredient": {
          "path": "ingredient",
          "fullPath": "/fr/administration/ingredient",
          "name": "Ingredient",
          "add": {
            "path": "ajouter",
            "fullPath": "/fr/administration/ingredient/ajouter",
            "name": "Ajouter un ingredient"
          }
        }
      }
    }
  },
```

1. Aller dans Web -> vue-app -> src -> router
2. Ouvrir le fichier index.ts
3. Ajouter une importation de votre component
4. Ajouter le chemin vers votre route avec le nom de votre component
 
```ts
import i18n from "@/i18n";
import {Role} from "@/types";
import {useMemberStore} from "@/stores/memberStore";
import {createRouter, createWebHistory} from "vue-router";

import Home from "../views/Home.vue";

import Admin from "../views/admin/Admin.vue";
import AdminBookIndex from "@/views/admin/AdminBookIndex.vue";
import AdminAddBookForm from "@/views/admin/AdminAddBookForm.vue";

// Ici pour le #2
import AdminAjoutIngredient from "@/views/admin/AdminAjoutIngredient.vue"

import AdminEditBookForm from "@/views/admin/AdminEditBookForm.vue";
import IngredientsIndex from "@/views/IngredientsIndex.vue";

const router = createRouter({
  // eslint-disable-next-line
  scrollBehavior(to, from, savedPosition) {
    // always scroll to top
    return {top: 0};
  },
  history: createWebHistory(),
  routes: [
    {
      path: i18n.t("routes.home.path"),
      name: "home",
      component: Home,
      },
    {
        path: i18n.t("routes.ingredients.path"),
        name: "ingredients",
        component: IngredientsIndex,
      },
    {
      path: i18n.t("routes.admin.path"),
      name: "admin",
      component: Admin,
      meta: {
        requiredRole: Role.Admin,
        noLinkInBreadcrumbs: true,
      },
      children: [
          {
              path: i18n.t("routes.admin.children.books.path"),
              name: "admin.children.books",
              component: Admin,
              children: [
                  {
                      path: "",
                      name: "admin.children.books.index",
                      component: AdminBookIndex,
                  },
                  {
                      path: i18n.t("routes.admin.children.books.add.path"),
                      name: "admin.children.books.add",
                      component: AdminAddBookForm,
                  },
                  {
                      path: i18n.t("routes.admin.children.books.edit.path"),
                      alias: i18n.t("routes.admin.children.books.edit.path"),
                      name: "admin.children.books.edit",
                      component: AdminEditBookForm,
                      props: true
                  },
              ],
          },

          //Ici pour le #3
          {
            path: i18n.t("routes.admin.children.ingredient.add.path"),
            name: "admin.children.ingredient.add",
              component: AdminAjoutIngredient,
          },
      ]
    }
  ]
});
```

7. Vous n'avez pas besoin d'ajouter rien d'autre pour que le lien se fasse, car la sous-barre de navigation pour l'admin est généré dynamiquement dans Web -> vue-app -> src -> components -> navigation -> subnav.vue
8. C'est le v-for child in directChildRoutes fait le travail.

```vue
<template>
  <div v-if="routeKey" class="subnav" :class="{ 'subnav--expand': isExpanded }">
    <button class="subnav__title" @click="toggleExpansion">
      {{ t(`routes.${routeKey}.name`) }}
      <IconChevron class="icon icon--white" :class="{'icon--rotate-180 icon--black' : !isExpanded}"/>
    </button>

    <Transition name="expand">
      <ul
        class="subnav__content"
        ref="content"
        v-show="isExpanded"
        v-if="directChildRoutes != null && directChildRoutes.length > 0"
      >
          <li v-for="child in directChildRoutes" :key="child.name">
            <RouterLink :to="getChildPath(routeKey, child.name?.toString())" class="subnav__navlink">{{ t(`routes.${child.name?.toString()}.name`) }}</RouterLink>
          </li>
      </ul>
    </Transition>
  </div>
</template>
```


[Retour à la documentation](/stackAccueil)