---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Projet intégrateur"
  text: "Site de cours pour Projet intégrateur"
  tagline: 
  actions:
  #  - theme: brand
  #    text: Cours 1
  #    link: /cours1
  #  - theme: brand
  #    text: Cours 2
  #    link: /cours2
  #  - theme: brand
  #    text: Cours 3
  #    link: /cours3
  #  - theme: brand
  #    text: Cours 4
  #    link: /cours4
  #  - theme: brand
  #    text: Cours 5
  #    link: /cours5
  #  - theme: brand
  #    text: Cours 6
  #    link: /cours6
  #  - theme: brand
  #    text: Cours 7
  #    link: /cours7
  #  - theme: brand
  #    text: Cours 13
  #    link: /cours13
features:

  - title: Stack
    link : /stackAccueil
    details: Documentation pour la stack SPK

---

