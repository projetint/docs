---
outline: deep
---
# Création du endpoint

Le endpoint permet de définir une route d'API, par exemple "/api/ingredients".

### Création du DTOs (Data Transfer Object)

1. Aller dans Web -> Features
2. Définir le bon niveau d'accès pour votre entité (Admins, Members ou Public) selon le rôle nécessaire pour intéragir avec votre entité.
3. Créer un dossier avec le nom de votre entité dans le dossier choisi en #2.
4. Dans ce dossier créer un fichier VotreEntiteDTO.cs
5. Codez une classe qui contiendra les attributs nécessaire pour la couche Web.

```cs
namespace Web.Features.Ingredients;

public class IngredientDto
{
    public Guid Id { get; set; }
    public string Nom { get; set; } = default!;
    public string Type { get; set; } = default!;
    public string Slug { get; set; } = default!;
}
```

### Création du mappage

5. Aller dans web -> Mapping -> Profiles
6. Ouvrir ResponseMappingProfile.cs
7. Cette classe permet de faire le mappage entre l'entité de Domain et le DTO que nous venons de créer.
8. Ajuster le code selon l'exemple suivant
9. Si notre fonctionnalité avait une requête, il faudrait aussi modifier RequestMappingProfile.cs

```cs
using Application.Common;
using AutoMapper;
using Domain.Entities;
using Domain.Entities.Books;
using Microsoft.AspNetCore.Identity;
using Web.Features.Admins.Books;
using Web.Features.Common;
using Web.Features.Ingredients;
using Web.Features.Members.Me.GetMe;

namespace Web.Mapping.Profiles;

public class ResponseMappingProfile : Profile
{
    public ResponseMappingProfile()
    {
        CreateMap<IdentityResult, SucceededOrNotResponse>();

        CreateMap<IdentityError, Error>()
            .ForMember(error => error.ErrorType, opt => opt.MapFrom(identity => identity.Code))
            .ForMember(error => error.ErrorMessage, opt => opt.MapFrom(identity => identity.Description));

        CreateMap<Member, GetMeResponse>()
            .ForMember(x => x.Roles, opt => opt.MapFrom(x => x.User.RoleNames))
            .ForMember(x => x.PhoneNumber, opt => opt.MapFrom(x => x.GetPhoneNumber()))
            .ForMember(x => x.PhoneExtension, opt => opt.MapFrom(x => x.GetPhoneExtension()));

        CreateMap<Book, BookDto>()
            .ForMember(bookDto => bookDto.Created, opt => opt.MapFrom(book => book.Created.ToDateTimeUtc()))
            .ForMember(bookDto => bookDto.NameFr, opt => opt.MapFrom(book => book.NameFr))
            .ForMember(bookDto => bookDto.NameEn, opt => opt.MapFrom(book => book.NameEn))
            .ForMember(bookDto => bookDto.DescriptionFr, opt => opt.MapFrom(book => book.DescriptionFr))
            .ForMember(bookDto => bookDto.DescriptionEn, opt => opt.MapFrom(book => book.DescriptionEn));

        CreateMap<Ingredient, IngredientDto>()
            .ForMember(IngredientDto => IngredientDto.Created, opt => opt.MapFrom(ingredient => ingredient.Created.ToDateTimeUtc()))
            .ForMember(IngredientDto => IngredientDto.Nom, opt => opt.MapFrom(ingredient => ingredient.Nom))
            .ForMember(IngredientDto => IngredientDto.Type, opt => opt.MapFrom(ingredient => ingredient.Type));
    }
}
```

### Création du endpoint
10. Dans le dossier que vous avez créer en #3
11. Créer un autre dossier avec le nom de votre fonctionnalité (LireTousIngredients par exemple)
12. Dans le dossier en #11, créer un fichier nommé VotreFonctionnalitéEndpoint.cs

13. Voici le contenu de mon fichier LireTousIngredientsEndpoint.cs

```cs
using Domain.Repositories;
using FastEndpoints;
using Microsoft.AspNetCore.Authentication.Cookies;
using IMapper = AutoMapper.IMapper;

namespace Web.Features.Ingredients.LireTousIngredients;

public class LireTousIngredientsEndpoint: EndpointWithoutRequest<List<IngredientDto>>
{
    private readonly IMapper _mapper;
    private readonly IIngredientRepository _ingredientRepository;

    public LireTousIngredientsEndpoint(IMapper mapper, IIngredientRepository ingredientRepository)
    {
        _mapper = mapper;
        _ingredientRepository = ingredientRepository;
    }

    public override void Configure()
    {
        DontCatchExceptions();

        Get("ingredients");
        Roles(Domain.Constants.User.Roles.MEMBER);        
        AuthSchemes(JwtBearerDefaults.AuthenticationScheme);
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var lesIngredients = _ingredientRepository.LireTous();
        await SendOkAsync(_mapper.Map<List<IngredientDto>>(lesIngredients), ct);
    }
}
```
14. Quelques clarifications
   1. On hérite de EndpointWithoutRequest car on n'a pas de requête dans un LireTous. Autrement on hériterait de Endpoint.
   2. C'est ici que se fait le lien entre la couche Web et les autres couches
      1. Accès à la couche application si on a du travail à faire
      2. Dans notre cas, on saute par-dessus et on va directement au Repository car un LireTous n'a pas de code applicatif à faire
   3. La ligne Roles(...) définit le type d'utilisateur qui peut utiliser cette route.
   4. Le Get("ingredients") est ce qui me permet de répondre aux appels à "/api/ingredient"
   5. Le _ingredientsRepository.LireTous() fait appel au repository créé précédemment, c'est précisément ici que la couche Web fait appel aux autres couches.
   6. Le _mapper.Map utilise le mapping profile que vous avez fait plus haut.
15. Aller valider dans Swagger que votre route d'API retourne bien les données de la BD
16. Être encore plus heureux!

[Retour à la documentation](/stackAccueil)