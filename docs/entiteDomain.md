---
outline: deep
---
# Création d'une entité dans Domain
  1. Dans Domain\Entities Ajouter une classe .cs au nom de votre entité
  2. Faire hériter la classe de AuditableAndSoftDeletableEntity
  3. Implémenter l'interface ISanitizable
  4. Ajouter les attributs que vous avez besoin.
  5. Ajouter une méthode nommée SanitizeForSaving() qui trim chacune des string.
  6. (Si nécessaire) Ajouter un champ de type string nommé Slug que vous utiliserez pour le routing.

```cs
public class NomDeMonEntite : AuditableAndSoftDeletableEntity, ISanitizable
{
    public string Attribut1 { get; set; } = default!;
    public decimal Attribut2 { get; set; } = default!;
    public double Attribut3 { get; set; } = default!;
    public int Attribut3 { get; set; } = default!;
    public DateTime Attribut4 { get; set; } = default!;
    //etc.
}
```

  5. Dans Persistence\NomDeAppDbContext.cs, ajouter un dbSet public pour votre entité

```cs
public DbSet<NomDeMonEntite> NomDeMonEntite { get; set; } = null!;
```

  6. [Exécuter la commande d'ajout de migration en donnant un nom significatif à la migration](/bdMigrations).
  7. Valider le contenu de la migration, vous devriez voir votre entité

Par exemple :
```cs
protected override void Up(MigrationBuilder migrationBuilder)
{
    migrationBuilder.CreateTable(
        name: "Ingredients",
        columns: table => new
        {
            Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
            Nom = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Slug = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Created = table.Column<DateTime>(type: "datetime2", nullable: false),
            CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
            LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
            LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
            Deleted = table.Column<DateTime>(type: "datetime2", nullable: true),
            DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
        },
        constraints: table =>
        {
            table.PrimaryKey("PK_Ingredients", x => x.Id);
        });
```

  8.  [Exécuter la commande de mise à jour de la bd](/bdMigrations.md).
  9.  Valider avec SSMS que la BD contient une table au nom de votre entité.
  10. Être heureux!

[Retour à la documentation](/stackAccueil)