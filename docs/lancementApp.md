---
outline: deep
---
# Lancement du front-end Vue à partir du terminal de VS
```bash
$ cd .\src\Web\vue-app
$ yarn build --watch
```

# Lancement du back-end .NET Core 8 à partir du terminal de VS (un autre terminal que le front-end)
```bash
$ cd .\src\Web
$ npm run dev
```


[Retour à la documentation](/stackAccueil)