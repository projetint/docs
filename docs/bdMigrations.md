---
outline: deep
---

# Ajout d'une migration à partir du terminal de VS

```bash
$ cd .\src\Persistence

$ dotnet ef migrations add {MigrationName} --startup-project ..\Web\
```

# Mise à jour de la BD à partir du terminal de VS

```bash
$ cd .\src\Persistence

$ dotnet ef database update --startup-project ../Web/
```

# Utilisateur par défaut
- Default user is `admin@gmail.com` with password `Qwerty123!`

# Instant
An instant represents a moment in time and is always in UTC. Hence, `InstantHelper.GetUtcNow()` will return UTC current date and time. 
However, if you parse a string to an Instant (using `ParseFromString` for example) the Instant will contain the same date as the string but it will be saved in UTC. 
For example, 
- If you parse the string `2023-10-12` to an Instant, the Instant object will contain `2023-10-12 00:00:00` and not `2023-10-11 20:00:00` (which is UTC equivalent of `2023-10-12 00:00:00`)
- But, `2023-10-11 20:00:00-04` will be saved in database
- But, when getting object from database after saving it, the Instant will be `2023-10-12 00:00:00` and not `2023-10-11 20:00:00`
- It is parsed automatically when getting object from database

In conclusion, you don't need to worry about the timezone when parsing a string to an Instant or when getting an Instant from database. The timezone will be your local timezone.
Also, you don't need (and should not) compare your object's Instant with `InstantHelper.GetUtcNow()` because the Instant will be in your local timezone and not in UTC.

#### Comparing object's Instant to current date
Although, your object's Instant will be in your local timezone, it will be saved in UTC. 

Here's a list of conversion methods offered with Instant and what they do to an Instant that contains `2023-10-10 00:00:00`
1. `ToDateTimeUtc()` : will return DateTime with `2023-10-10 00:00:00`
2. `ToDateTimeOffset()` : will return DateTime and offset with `2023-10-10 00:00:00 +00:00`

So to compare your object's instant to current date time, I suggest doing `myObject.ItsInstant.ToDateTimeUtc() < DateTime.Now`


[Retour à la documentation](/stackAccueil)