---
outline: deep
---
# Cours 5
# Planification
<!-- no toc -->
- [Planification](#planification-hebdo)
- [SCRUM](#scrum)

# Planification hebdo

- Remise Tp1 mercredi
- Dernière semaine de préparation!!
- Visite de Rose mercredi à 13h
- Je m'attends à ce que lundi 11h de la semaine prochaine tous les étudiants soient en mesure de commencer le code.

# SCRUM
- Tp1
  - Comment va le rapport ? Des questions ?
  - Premier regard sur votre pivotal.
- Rôles
  - Azure
  - Module par personne