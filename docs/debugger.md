---
outline: deep
---

# Comment déboguer

### App vue

Pour tout ce qui se trouve sous Web -> vue-app, vous déboguez comme dans les cours de Web
1. Des console.log
2. Les outils de développeur de votre navigateur
   
### API et backend

Pour tout ce qui n'est pas dans votre app vue, vous déboguez comme dans les cours de Prog
1. Lancer votre app (Frontend et Backend)
2. Retourner dans Visual Studio
3. Dans le menu sélectionner Déboguer -> Attacher au processus
4. Dans la fenêtre des processus disponibles
   1. Écrire Web.e dans la case de filtre à droite
   2. Sélectionner Web.exe
   3. Cliquer sur attacher
5. Retourner dans votre app et faîtes l'action pour atteindre votre point d'arrêt
6. Bon débogage :) 

### Swagger

La stack utilise Swagger afin de vous permettre de valider vos appels d'API sans utiliser le front-end. Vous pouvez en tout temps consulter le localhost:7101/swagger/ pour valider les fonctionnalités de votre API. Ça permet de découper le déboggage en deux gros blocs. Si le résultat attendu sur Swagger est le bon, alors l'erreur est dans l'app Vue, sinon l'erreur dans le back-end.

Pour faciliter le déboggage avec Swagger, vous pouvez remplacer temporairement les lignes suivantes dans la méthode Configure de votre Endpoint :

```cs
      Roles(Domain.Constants.User.Roles.MEMBER);
      AuthSchemes(JwtBearerDefaults.AuthenticationScheme);
```

par cette ligne :

```cs
      AllowAnonymous();
```

Ça évitera les erreurs 401 et vous pourrez valider le contenu. N'oubliez pas de rechanger les lignes une fois que vous avez validé, sinon votre route ne sera pas sécurisée.
[Retour à la documentation](/stackAccueil)