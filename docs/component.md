---
outline: deep
---
# Création du component

1. Aller dans Web -> vue-app -> src -> views
2. Créer un fichier nommé VotreComponent.vue
3. Vous êtes probablement meilleur que moi en vue, j'ai environ 5 heures d'xp. C'est essentiellement une copie d'un component de Rose que j'ai ajusté pour faire afficher l'ensemble de mes ingrédients.
4. Alors, voici mon code, mais vous ferez beaucoup mieux!

```vue
<template>
    <div class="content-grid content-grid--subpage content-grid--subpage-table">
        <div class="content-grid__header">
            <h1 class="back-link-title">{{ t(`routes.ingredients.name`) }}</h1>
            <div class="content-grid__filters">
                <SearchInput v-model="searchValue" />
            </div>
        </div>
        <Card>
            <DataTable :headers="ingredientHeader"
                       :search-value="searchValue"
                       :is-loading="enChargement"
                       :items="tableIngredients" />
        </Card>
    </div>
</template>

<script lang="ts" setup>
    import { useI18n } from "vue3-i18n";
    import { computed, onMounted, ref } from "vue";
    import { useIngredientService } from "@/inversify.config";
    import Card from "@/components/layouts/items/Card.vue";
    import SearchInput from "@/components/layouts/items/SearchInput.vue";
    import DataTable from "@/components/layouts/items/DataTable.vue";
    import { Ingredient } from "@/types/entities";

    const { t } = useI18n()
    const ingredientService = useIngredientService()

    const lesIngredients = ref<Ingredient[]>([]);
    const searchValue = ref("");
    const enChargement = ref(false);

    const tableIngredients = computed(() => {
        return lesIngredients.value.map((x: Ingredient) => {
            return {
                id: x.id,
                nom: (x.nom) ?? '',
                description: x.description
            }
        }).sort((a: any, b: any) => {
            if (a.nom < b.nom) {
                return -1;
            }
            if (a.nom > b.nom) {
                return 1;
            }
            return 0;
        }) as Ingredient[];
    })

    onMounted(async () => {
        await ChargerIngredients();
    });

    async function ChargerIngredients() {
        enChargement.value = true;

        let ingredients = await ingredientService.LireTousIngredients();
        console.log(ingredients);
        if (ingredients && ingredients.length > 0) {
            lesIngredients.value = ingredients;
        }
        enChargement.value = false;
    }

    const ingredientHeader = [
        {
            text: t("ingredient.nom"),
            value: 'nom',
            sortable: true,
            width: 300,
        },
        {
            text: t("ingredient.description"),
            value: "description",
            sortable: true,
            width: 150,
        }
    ];
</script>
```

[Retour à la documentation](/stackAccueil)