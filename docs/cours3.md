---
outline: deep
---
# Cours 3
# Planification
<!-- no toc -->
- [Pivotal Tracker](#pivotal-tracker)
- [SCRUM](#scrum)
- [À faire par personne](#à-faire-par-personne)

# Pivotal Tracker

- Créer un compte sur Pivotal Tracker : https://www.pivotaltracker.com/signup/new
- Attendre que je vienne vous voir pour la suite.

# SCRUM
- Quel est le principal objectif de votre application ? Une seule phrase.
- Nommez tous les modules de votre application.
- Dessinez votre page d'accueil.

# À faire par personne

- Install latest SDK of .NET Core 6 [here] (https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
- Restore nuget package
- Install [Sql Server](https://www.microsoft.com/en-ca/sql-server/sql-server-downloads)
- Install [SSMS] (https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16)
  - Create dev user with rights to create tables :
    - In `Security` folder, right click on `Logins` and select `New login`
    - Select SQL Server authentication
    - Login name : `dev`
    - Password : `dev`
    - Click OK
- Install nvm
- Install node 18.16.1
    ```bash
    $ nvm install 18.16.1
    $ nvm use 18.16.1
    ```
