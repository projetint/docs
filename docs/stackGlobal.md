---
outline: deep
---

# StackTechno

- MSSQL
- .NET 8
- C#
- Vue 3

# Documentation Microsoft Entity Framework Core

https://learn.microsoft.com/en-us/ef/core/

# Architecture

- [REPR desing pattern](https://deviq.com/design-patterns/repr-design-pattern) 
- Vertical slices
- Architecture multi-couches avec 5 projets dans la solution
  - Infrastructure
    - C'est juste un entrepot qui stock les données
    - Contient les Repository
    - Un repository est l'accès au contenu de la BD. Vous pouvez voir un repository comme un entrepot qui permet de récupérer, stocker, supprimer ou modifier des données (CRRUD).
    - Il ne doit y avoir aucune logique dans les repository
  - Persistence
    - Info de la Bd (Sql, Mongo ou autre)
    - Le context de la bd
    - Les migrations (créer selon les entités)
    - Les seeds (contenu initial dans la bd)
  - Domain
    - Les objets (entités)
    - Ne doit jamais dépendre des autres couches et doit être protégée des autres couches
    - Les autres couches l'utilisent et n'utilise aucune autre couche
  - Application
    - Logique Backend
    - Couche des services
    - Un service par cas d'utilisation
  - Web
    - Couche UI
    - Une app .NET pour l'authentification
    - Une app Vue.Js une fois l'authentification passée

![Architecture stack SPK](/images/archGlobal.png)

[Retour à la documentation](/stackAccueil)
