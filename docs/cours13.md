---
outline: deep
---
# Cours 13
# Planification
<!-- no toc -->
- [Un peu de théorie pédagogique](#théorie-de-la-pédagogie)
- [Plan machiavélique](#plan-machiavélique)
- [Rétro Tp1](#rétro-tp1)
- [Les dépôts](#les-dépôts)
- [SCRUM](#scrum)
  
# Théorie de la pédagogie

### Zone proximale de développement (ZPD)

- Théorie de Vygotsky (https://fr.wikipedia.org/wiki/Zone_proximale_de_d%C3%A9veloppement)
- La zone de ce qu'une personne peut apprendre avec l'aide d'une autre personne plus experte.
- La zone proximale commence à la limite de ce que la personne peut faire par elle-même et se termine à la limite de ce qu'une personne peut faire avec de l'aide.

### Sentiment d'efficacité personnelle (SEP)

- Théorie de Bandura (https://fr.wikipedia.org/wiki/Auto-efficacit%C3%A9)
- Le SEP c'est l'évaluation de notre aptitude dans un domaine donné.
- Est-ce que je me sens en contrôle de ma propre réussite ? C'est moi qui contrôle ma réussite ou c'est le prof ?
- Si c'est moi, augmentation de :
  - Efforts
  - Persévérance
  - Résilience
  - Réussite
  - Ambition
- Comment obtenir du SEP :
  1. Expériences de maîtrise (seulement si on attribue le succès à des éléments qu'on contrôle)
  2. Apprentissage social
    - Plus le sujet est proche de nous (facteurs sociaux, compétences, ...), plus l'apprentissage social fonctionne et plus il est susceptible d'être source de SEP.

# Plan machiavélique

1. De vous garrocher en groupe dans votre ZPD
2. De vous aider juste assez pour ne pas que vous quittiez la ZPD
3. Ne pas vraiment vous laisser le choix de collaborer
4. Que vous réalisiez ce que vous êtes en mesure de faire et que vous vous sentiez en contrôle de votre réussite (pas seulement celle scolaire)
5. De générer une tonne de SEP en développement logiciel (mon but ultime)
  
# Rétro Tp1

- Au délà de tout ce que je pouvais espérer
- Mercredi 21 février

# Les dépôts

- https://gitlab.com/deptinfo.cegepgarneau.ca/dashboardprogramme
- https://gitlab.com/deptinfo.cegepgarneau.ca/grilleeval
- https://gitlab.com/deptinfo.cegepgarneau.ca/jardinsentente
- https://gitlab.com/deptinfo.cegepgarneau.ca/thibaultantoine
  
# SCRUM
