---
outline: deep
---
# Ajout de la route dans la couche Web

### Création de la route

1. Aller dans Web -> vue-app -> src -> locales
2. Ouvrir le fichier fr.json (vous n'aurez pas besoin de faire la version anglaise de votre site)
3. Ajouter la route que vous voulez créer

Dans l'exemple, je place ma route à l'extérieur d'admin, car ce n'est pas une fonctionnalité réservée aux admins.
```json
{
  "routes": {
    "login": {
      "path": "/connexion",
      "fullPath": "/connexion",
      "name": "Connexion"
    },
    "ingredients": {
      "path": "ingredients",
      "fullPath": "/fr/ingredients",
      "name": "Les ingredients"
    },
```

4. Tandis que nous sommes dans le fichier fr.json, nous pouvons en profiter pour définir les valeurs à afficher pour l'entité ingrédient
5. Descendre vers la moitié du fichier, après les messages d'erreur et ajouter un bloc pour l'entité ingrédient
```json
  "book": {
    "info": "Informations du livre",
    "title": "Titre",
    "titleFr": "Titre - FR",
    "titleEn": "Titre - EN",
    "summary": "Résumé",
    "summaryFr": "Résumé - FR",
    "summaryEn": "Résumé - EN",
    "isbn": "ISBN",
    "author": "Auteur",
    "editor": "Éditeur",
    "yearOfPublication": "Année de publication",
    "numberOfPages": "Nombre de pages"
  },
  "ingredient": {
    "nom": "Nom de l'ingredient",
    "description": "Description de l'ingredient"
  },
```

6. Aller dans Web -> vue-app -> src -> router
7. Ouvrir le fichier index.ts
8. Importer votre component
9.  Ajouter le chemin vers votre route avec le nom du fichier de votre component
 
```ts
import LireTousIngredient from "@/views/LireTousIngredient.vue";

const router = createRouter({
  // eslint-disable-next-line
  scrollBehavior(to, from, savedPosition) {
    // always scroll to top
    return {top: 0};
  },
  history: createWebHistory(),
  routes: [
    {
      path: i18n.t("routes.home.path"),
      name: "home",
      component: Home,
      },
    {
        path: i18n.t("routes.ingredients.path"),
        name: "ingredients",
        component: LireTousIngredient,
    },
```

7. Aller dans Web -> vue-app -> src -> components -> navigation
8. Ouvrir le fichier AdminNavBarItems.vue
9. Ajouter un routerlink pour votre route

```vue
<template>
    <ul class="navbar__nav">
        <li>
            <RouterLink :to="t('routes.home.path')" class="navbar__navlink">{{ t('routes.home.name') }}</RouterLink>
        </li>
        <li>
            <RouterLink :to="t('routes.ingredients.path')" class="navbar__navlink">{{ t('routes.ingredients.name') }}</RouterLink>
        </li>
        <li>
            <Subnav :routeKey="'admin'" />
        </li>
        <li>
            <button class="navbar__navlink" @click="logout">{{ t('global.logout') }}</button>
        </li>
    </ul>
</template>
```
10. Aller dans web -> Controllers -> IntranetControllers.cs
11. Ajouter votre route
```cs
// FRENCH
[Route("/connexion")]
[Route("/authentification-a-deux-facteurs")]
[Route("/mot-de-passe-oublie")]
[Route("/reinitialiser-mot-de-passe")]
[Route("/mon-compte")]
[Route("/administration/membres")]
[Route("/administration/membres/ajouter")]
[Route("/livres")]
[Route("/ingredients")]

```

Normalement, (peut-être) que vous devriez voir ceci :

![Succès](/images/LireTousIng.png)

11.  Être fier!

[Retour à la documentation](/stackAccueil)