---
outline: deep
---
# Architecture de la couche Web et CSS


### Architecture de la couche Web

La couche Web de la stack est divisé en deux parties.
1. Une première partie qui contient 2 éléments principaux
   1. L'API (le dossier Feature qui contient les endpoints)
   2. L'authentification en pages .cshtml (les dossiers Views, ViewModels et Controllers). Lorsque l'utilisateur se connecte, l'app Vue est lancée et on ne revient pas dans le .cshtml tant que l'utilisateur est connecté.
2. L'app vue (tout ce qui est dans le dossier vue-app)

Normalement, vous n'aurez pas beaucoup de modifications à faire dans les pages .cshtml. Vous travaillerez principalement dans l'app Vue et dans l'API.

### CSS
Le CSS de la stack est contenu dans les dossiers Sass. Il est codé en scss afin de permettre l'indentation des balises css.

Il y a deux dossiers Sass.
1. Un premier dans Web -> Sass qui contient le css pour l'authentification et les fichiers .cshtml
2. Un deuxième dans Web -> vue-app -> src -> Sass qui contient le css pour l'app vue.

### Pour ajouter du CSS

1. Créer un nouveau fichier .scss dans le dossier Sass (au besoin vous faire une structure de dossier pour classer vos fichiers .scss)
2. Ajouter vos règles de style dans le fichier .scss
3. Dans le fichier index.scss à la racine du dossier Sass, ajouter un import pour votre fichier.
  
Attention, les dossiers Sass doivent être recompilés pour que les changements soient pris en compte, ils ne sont pas rechargés automatiquement (hot reload). Pour ce faire, vous devez arrêter votre backend et refaire la commande npm run dev.

Par exemple, si vous voulez changer le design de la navbar, vous devez aller dans Sass -> blocks -> _navbar.scss
```scss
.navbar {
  &__title,
  &__drawer-btn {
    width: calc(100% + #{$page-padding-small} * 2);
    margin: 0 - $page-padding-small;
    padding: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 700;
    font-family: $font-montserrat;
  }

  &__title {
    background-color: $color-white;
    font-size: rem(13);
    line-height: rem(16);
    border-top: 1px solid $color-green;
  }
  }
}
```

[Retour à la documentation](/stackAccueil)