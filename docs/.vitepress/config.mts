import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base:"/docs/",
  title: "Projet intégrateur",
  description: "site de cours pour Projet intégrateur",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Accueil', link: '/' }
    ],

    sidebar: [
      {
        
      }
    ],

  }
})
