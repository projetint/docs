---
outline: deep
---
# Création du component

### Création du form

1. Aller dans Web -> vue-app -> src -> components
2. Si ce n'est déjà fait, créer un dossier au nom de votre entité.
3. Dans le dossier crée en #2, ajouter un fichier .vue qui contiendra le formulaire pour votre entité.
4. Voici mon formulaire super complexe /s

```vue
<template>
    <form class="form"
          novalidate
          @submit.prevent="handleSubmit"
          enctype="multipart/form-data">
        <Fieldset :title="props.title">
            
                <FormRow>
                    <FormInput v-model="ingredient.nom"
                               type="text"
                               name="nom"
                               :ref="addFormInputRef"
                               :label="t('ingredient.nom')"
                               @validated="handleValidation" />
                    <FormInput v-model="ingredient.type"
                               type="text"
                               name="type"
                               :ref="addFormInputRef"
                               :label="t('ingredient.type')"
                               @validated="handleValidation" />
                </FormRow>
            
        </Fieldset>

        
            <button class="form__submit btn btn--fullscreen" type="submit">
                {{ t("global.save") }}
            </button>
    </form>
</template>

<script lang="ts" setup>
import FormRow from "@/components/forms/FormRow.vue"
import FormInput from "@/components/forms/FormInput.vue"
import ProductBaseForm from "@/components/products/ProductBaseForm.vue"
import {ref} from "vue"
import {useI18n} from "vue3-i18n"
import {notifyError} from "@/notify"
    import { Status } from "@/validation"
    import Fieldset from "@/components/forms/Fieldset.vue";
import {Ingredient} from "@/types/entities"

// eslint-disable-next-line no-undef
const props = defineProps<{
    ingredient?: Ingredient
}>()

// eslint-disable-next-line
const emit = defineEmits<{
    (event: "formSubmit", ingredient: Ingredient): void
}>()

const { t } = useI18n()

    const ingredient = ref<Ingredient>(props.ingredient ?? {})

const formInputs = ref<any[]>([])
const inputValidationStatuses:any = {}

function addFormInputRef(ref: typeof FormInput) {
    if (!formInputs.value.includes(ref) && ref)
        formInputs.value.push(ref)
}

async function handleValidation(name: string, validationStatus: Status) {
    inputValidationStatuses[name] = validationStatus.valid
}

async function handleSubmit() {
    formInputs.value.forEach((x: typeof FormInput) => x.validateInput())
    if (Object.values(inputValidationStatuses).some(x => x === false)) {
        notifyError(t('global.formErrorNotification'))
        return
    }
    emit("formSubmit", ingredient.value)
}
</script>
```

### Création de la vue
4. Aller dans Web -> vue-app -> src -> views
5. Créer un fichier nommé VotreComponent.vue
6. Ajouter le formulaire précédemment construit
7. Ajuster le code.

```vue
<template>
    <div class="content-grid content-grid--subpage">
        <Breadcrumbs :title="t(`routes.admin.children.ingredient.add.name`)" />
        <BackLinkTitle :title="t(`routes.admin.children.ingredient.add.name`)" />
        <Card>
            <IngredientForm @formSubmit="handleSubmit" />
        </Card>
    </div>
</template>

<script lang="ts" setup>
import Breadcrumbs from "@/components/layout/Breadcrumbs.vue";
import BackLinkTitle from "@/components/layout/BackLinkTitle.vue";
import IngredientForm from "@/components/ingredient/IngredientForm.vue";
import Card from "@/components/layout/Card.vue";
import {useI18n} from "vue3-i18n";
import {Ingredient} from "@/types/entities";
import {notifyError, notifySuccess} from "@/notify";
import { useIngredientService } from "@/inversify.config";
import {ICreateIngredientRequest} from "@/types/requests/createIngredientRequest";
import {useRouter} from "vue-router";

const {t} = useI18n()
const router = useRouter();
const ingredientService = useIngredientService();

    async function handleSubmit(ingredient: Ingredient) {
        let succeededOrNotResponse = await ingredientService.CreerIngredient(ingredient as ICreateIngredientRequest)
    if (succeededOrNotResponse && succeededOrNotResponse.succeeded) {
        notifySuccess(t('validation.ingredient.add.success'))
        setTimeout(() => {
            router.back();
        }, 1500);
    } else {
        let errorMessages = succeededOrNotResponse.getErrorMessages('validation.ingredient.add');
        if (errorMessages.length == 0)
            notifyError(t('validation.ingredient.add.errorOccured'))
        else
            notifyError(errorMessages[0])
    }
}
</script>
```

[Retour à la documentation](/stackAccueil)