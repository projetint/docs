---
outline: deep
---
# Documentation pour la stack SPK
<!-- no toc -->
- [Stack et architecture](/stackGlobal)
- [Installation](/installation)
- [Lancement de l'app](/lancementApp)
- [Bd et Migrations](/bdMigrations)
- [Comment deboguer](/debugger.md)
- [Architecture de la couche Web et css](/webCSS.md)
- Tutoriels
    - [Création d'une entité dans Domain](/entiteDomain)
    - [Création d'une liaison dans Domain](/liaison.md)
    - [Ajout de données initiales dans la Bd](/seed.md)
    - Création d'un lireTous d'une entité
      - [Création du repository pour une entité](/repository.md)
      - Comme le lireTous est un accès direct dans la bd sans traitement, on n'ajoute pas de contenu dans la couche Applications. La couche Web appellera directement le repository.
      - Couche Web
        - [Création du endpoint](/endpoint.md)
        - [Création du service de votre entité](/serviceWeb.md)
        - [Création du component](/component.md)
        - [Ajout de la route dans la couche Web](/lienNavbar.md)
<!--    - Création d'un Créer d'une entité
      - [Ajustement du repository pour une entité](/repositoryCreate.md)
      - Couche Application
        - [Création du service applicatif](/serviceAppCreer.md)
      - Couche Web
        - [Création du endpoint, de la requête et du validateur](/endpointCreer.md)
        - [Création du service de la couche Web](/serviceWebCreer.md)
        - [Création du component](/componentCreer.md)
        - [Ajout de la route](/lienNavbarCreer.md)
-->
