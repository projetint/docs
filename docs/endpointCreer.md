---
outline: deep
---
# Création du endpoint

### Création du endpoint
1. Dans Web -> Features
2. Si vous n'avez pas de dossier au nom de votre entité, en créer un
3. Dans Web -> Features -> VotreEntité
4. Créer un dossier nommé CréerVotreEntité
5. Dans le dossier en #4 créer 3 fichiers
   1. CréerVotreEntitéEndpoint.cs
   2. CréerVotreEntitéRequest.cs
   3. CréerVotreEntitéValidator.cs

### Le fichier request

- Les endpoint avec requête (creer, supprimer, modifier) n'utilise pas le DTO de votre entité. Ainsi, il faut créer un objet pour chacune de ces requêtes.
- Le fichier de requête contiendra les informations saisie dans le formulaire Web.

```cs
public class CreerIngredientRequest
{
    public string Nom { get; set; } = default!;
    public string Type { get; set; } = default!;
}
```


### Le fichier validator

- Le fichier validator contient les validations qui seront effectuées sur la requête

```cs
public class CreerIngredientValidator:  Validator<CreerIngredientRequest>
{
    public CreerIngredientValidator()
    {
        RuleFor(x => x.Nom)
            .NotNull()
            .NotEmpty()
            .WithErrorCode("InvalideNom")
            .WithMessage("Le nom d'un ingredient ne peut être null ou vide.");

        RuleFor(x => x.Type)
            .NotNull()
            .NotEmpty()
            .WithErrorCode("InvalideType")
            .WithMessage("Le type d'un ingredient ne peut être null ou vide.");
    }
}
```

### Le endpoint

- C'est le point d'entrée de l'API, dans l'exemple suivant il est appelé quand une requête est faite sur api/ingredients grâce à Post(ingredients) dans la méthode de configuration.
- La méthode Roles permet de définir les autorisations nécessaires pour accèder à ce endpoint.
- La méthode HandleAsync est celle qui défini le comportement du endpoint, dans l'exemple suivant
  - On mappe la requête reçu dans un objet du domain
  - On appelle le service applicatif avec l'objet qu'on a mappé
  - On renvoit une réponse

```cs
public class CreerIngredientEndpoint: Endpoint<CreerIngredientRequest, SucceededOrNotResponse>
{
    private readonly IMapper _mapper;
    private readonly IIngredientCreationService _ingredientCreationService;

    public CreerIngredientEndpoint(IMapper mapper, IIngredientCreationService bookCreationService)
    {
        _mapper = mapper;
        _ingredientCreationService = bookCreationService;
    }

    public override void Configure()
    {
        AllowFileUploads();
        DontCatchExceptions();

        Post("ingredients");
        Roles(Domain.Constants.User.Roles.ADMINISTRATOR);
        AuthSchemes(CookieAuthenticationDefaults.AuthenticationScheme);
    }

    public override async Task HandleAsync(CreerIngredientRequest req, CancellationToken ct)
    {
        var ingredient = _mapper.Map<Ingredient>(req);
        await _ingredientCreationService.CreerIngredient(ingredient);
        await SendOkAsync(new SucceededOrNotResponse(true), ct);
    }
}
```

### Création du mapping de la requête

1. Aller dans Web -> Mapping -> Profiles
2. Ouvrir RequestMappingProfile.cs
3. Ajouter une instruction CreateMap pour votre requete
   
- Si votre entité n'a pas de jointure, vous pourrez la mapper simplement comme Rose l'a fait avec book dans l'exemple.
- Si vous avez une jointure, vous aurez probablement besoin d'ajouter un ignore comme je l'ai fait pour la liaison ingrédient -> IngrédientRecette

```cs
public class RequestMappingProfile : Profile
{
    public RequestMappingProfile()
    {
        CreateMap<TranslatableStringDto, TranslatableString>().ReverseMap();

        CreateMap<CreateBookRequest, Book>();

        CreateMap<CreerIngredientRequest, Ingredient>().ForMember(opt => opt.IngredientRecettes, opt => opt.Ignore());

        CreateMap<EditBookRequest, Book>();
    }
}
```


[Retour à la documentation](/stackAccueil)