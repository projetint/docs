---
outline: deep
---
# Cours 7
# Planification
<!-- no toc -->
- [Quelques rappels](#quelques-rappels)
- [Vos premiers récits](#vos-premiers-récits)
- [SCRUM](#scrum)
# Quelques rappels

- Tout est fait en fonction de votre développement
  - Les clients sont au service de votre apprentissage
  - Ce n'est pas un Tp, vous n'avez pas à terminer le projet
- 0-8-5
  - Environ 12 heures de pratique par semaine (si on est généreux et qu'on se garde 1 heure de SCRUM)
  - 12 heures de projet réel, sans contenu théorique précis
- Un groupe de personnes qui collaborent ensemble pour s'aider à devenir meilleur
  - Il est impossible de plagier, les projets n'ont jamais été faits et chaque équipe à un projet différent
  - Collaborer autant que possible avec tous les autres étudiants

# Vos premiers récits
- JardinsEntente
  - Un utilisateur consulte son lot (lireUn sur l'entité Lot)
  - Un administrateur consulte la liste des utilisateurs (lireTous sur l'entité Utilisateur)
  - Un invité s'ajoute à la liste d'attente (créé sur l'entité PersonneAttente)
  - Un utilisateur consulte les règles des jardins (lireTous sur l'entité Règles)
- DashboardProgramme
  - Un administrateur ajoute un programme (créé sur l'entité Programme)
  - Un administrateur consulte la liste des utilisateurs (lireTous sur l'entité Utilisateur)
  - Un utilisateur consulte une session de son programme (lireUn entité Session)
  - Un utilisateur consulte un cours (lireUn entité Cours)
  - Un utilisateur consulte un groupe (lireUn entité Groupe)
- GrilleEval
  - Un administrateur crée un nouveau compte utilisateur (créé sur l'entité Utilisateur)
  - Un utilisateur crée une nouvelle grille d'évaluation (créé sur l'entité Grille)
  - Un utilisateur complète une grille pour une évaluation d'un étudiant (créé sur l'entité GrilleEtudiant)
  - Un utilisateur consulte la liste de ses groupes (lireTous sur l'entité Groupe)
  - Un utilisateur consulte une évaluation (lireUn sur l'entité Évaluation)
  
# SCRUM
